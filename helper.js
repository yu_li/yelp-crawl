var fs = require('fs');
var path = require('path');

var rask = require('rask');
var log = rask.log.get(module);

exports.stepByStep = function(initState, stepProvider) {
  var state = initState || {};
  state.startcwd = process.cwd();
  require('async').series(stepProvider(state), function(err) {
    if (err) {
      log.error(err);
      process.exit(1);
    }
    log.info('success, done!');
    process.exit(0);
  });
};

exports.randomUserAgent = (function() {
  var allAgents = [
    "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36",
    "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20130328 Firefox/22.0",
    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)",
    "Mozilla/5.0 (compatible; MSIE 10.0; Macintosh; Intel Mac OS X 10_7_3; Trident/6.0)",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
  ];
  return function() {
    var index = Math.floor((Math.random() * allAgents.length) + 0);
    return allAgents[index];
  };
})();

exports.genArray = function(stop, start, step) {
  var a = [];
  var i = start || 0;
  var s = step || 1;
  for (; i<stop; i=i+s) { a.push(i); }
  return a;
};

exports.ensureDir = function(dir, callback) {
  (function ensureDir(dir, mode, callback) {
    mode = mode || 0777 & (~process.umask());
    callback = callback || function () {};
    fs.exists(dir, function (exists) {
      if (exists) { return callback(null); }
      var current = path.resolve(dir), parent = path.dirname(current);
      ensureDir(parent, mode, function (err) {
        if (err) { return callback(err); }
        console.log('will make:', current);
        fs.mkdir(current, mode, function (err) {
          if (err && err.code != 'EEXIST') { return callback(err); } // avoid the error under concurrency
          callback(null);
        });
      });
    });
  })(dir, null, callback);
};

exports.execCmd = function(cmdstr, options, callback, hooks) {
  var child = require('child_process').exec(cmdstr, options || null);
  var out = '';
  var err = '';
  // first default hooks
  if (child.stdout) { child.stdout.on('data', function(buffer) { out += buffer.toString(); }); }
  if (child.stderr) { child.stderr.on('data', function(buffer) { err += buffer.toString(); }); }
  child.on('close', function(code) { callback(code, out, err); });
  // then customized hooks
  hooks && hooks(child);
};

exports.parseHtmlFile = function(path, callback) {
  var env = require('jsdom').env;
  require('fs').readFile(path, function(err, data) {
    if (err) {
      return callback(err);
    }
    var html = data.toString();
    env(html, function(err, window) {
      if (err) { return callback(err); }
      callback(null, require('jquery')(window));
    });
  });
};
