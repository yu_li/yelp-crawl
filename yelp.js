var fs = require('fs');
var path = require('path');
var util = require('util');

var async = require('async');
var rask = require('rask');
var log = rask.log.get(module);
var helper = require('./helper');

function getWgetCmd(url, outFileName) {
  //var tpl = 'wget --user-agent="%s" "%s" -O "%s" >/dev/null';
  //var tpl = path.resolve(__dirname, 'myget.sh') + ' http://proxylistpro.com "%s" >"%s"';
  var tpl = path.resolve(__dirname, 'myget.sh') + ' http://www.proxfree.com "%s" >"%s"';
  return util.format(tpl, url, outFileName);
}

exports.crawl = function(conf) {
  log.info('start process:', conf);
  helper.stepByStep({
      retry: []
    },
    function(state) {
      return [
        // ensure var/name and cd to it
        function(next) {
          var dir = util.format('./raw/%s/', conf['name']);
          log.info('ensure dir:', dir);
          helper.ensureDir(dir, function(err) {
            if (err) { return next(err); }
            process.chdir(dir);
            next();
          });
        },
        // pilot, to get the first page, extract total
        function(next) {
          var yelpUrl = util.format('http://www.yelp.com/search?%s', encodeURI(conf['q']));
          state.baseUrl = yelpUrl;
          var cmdstr = getWgetCmd(yelpUrl, 'pilot.html');
          log.info('will wget,', cmdstr);
          helper.execCmd(cmdstr, null, function(code, out, err) {
            if (code === 0) {
              log.info('wget done, now parse and analysis.');
              helper.parseHtmlFile('pilot.html', function(err, $) {
                if (err) { return next(err); }
                // totally based on experience
                var pagestr = $('div.page-of-pages').text().trim();
                var r = /Page\s*(\d+)\s*of\s*(\d+)/g.exec(pagestr);
                if (r) {
                  state.total = parseInt(r[2]);
                  log.info('got total page:', state.total);
                  next();
                } else {
                  next(new Error(util.format('try to find total page in pilot.html failed: %j.', r)));
                }
              });
            } else {
              next(new Error(util.format('wget failed: %d, %j.', code, err)));
            }
          });
        },
        // now download them one by one
        function(next) {
          var jobs = [];
          var yelpPageSize = 10;
          helper.genArray(state.total).forEach(function(i) {
            jobs.push(function(next) {
              log.info('start crawl page', i, 'of', state.total);
              var cmdstr = getWgetCmd(util.format('%s&start=%d', state.baseUrl, i*yelpPageSize), util.format('data-%d.html', i));
              helper.execCmd(cmdstr, null, function(code, out, err) {
                if (code === 0) {
                  log.info('succeeded crawling:', i);
                  next();
                } else {
                  log.error('fail crawling:', i, 'write to retry script.');
                  state.retry.push(cmdstr);
                  next();
                }
              });
            });
          });
          async.parallelLimit(jobs, conf['parallelLimit'] || 10,
            function(err, results) {
              if (err) {
                return next(err);
              }
              next();
            });
        },
        // if there is retry items, write them out
        function(next) {
          if (state.retry.length > 0) {
            log.info('output retry.sh.');
            fs.writeFile('retry.sh', state.retry.join('\n'), { mode: 493 }, next);
          } else {
            log.info('no retry items, skip output retry.sh.');
          }
        }
      ];
    });
};

exports.extract = function(conf) {
  log.info('start process:', conf);
  helper.stepByStep({
      dst: 'result.txt',
      retry: []
    },
    function(state) {
      return [
        // ensure dir data/<name>, and chdir to it
        function(next) {
          var dir = util.format('./data/%s/', conf['name']);
          log.info('ensure dir:', dir);
          helper.ensureDir(dir, function(err) {
            if (err) { return next(err); }
            process.chdir(dir);
            next();
          });
        },
        // find all raw files
        function(next) {
          if (conf['filelist']) {
            // user given filelist
            state.filelist = conf['filelist'];
            log.info('use user given filelist', state.filelist);
            next();
          } else {
            var p = path.resolve(state.startcwd, 'raw', conf['name']);
            log.info('will generate filelist:', p);
            fs.readdir(p, function(err, files) {
              if (err) { return next(err); }
              state.filelist = files.filter(function(file) { return file.substr(0, 5) === 'data-'; });
              log.info('found', state.filelist.length, 'files');
              next();
            });
          }
        },
        // parse and extract one by one
        function(next) {
          var jobs = [];
          state.filelist.forEach(function(file) {
            jobs.push(function(next) {
              var p = path.resolve(state.startcwd, 'raw', conf['name'], file);
              log.info('start extract page', p);
              var result = [];
              helper.parseHtmlFile(p, function(err, $) {
                var resultDivs = $('div.search-result.natural-search-result.biz-listing-large');
                resultDivs.each(function(index, res) {
                  var r = $(res);
                  result.push({
                    'biz-name': r.find('a.biz-name').text().trim(),
                    'address': r.find('address').text().trim(),
                    'biz-phone': r.find('span.biz-phone').text().trim(),
                    'category-str-list': r.find('span.category-str-list').text().trim()
                  });
                });
                log.info('extract page:', file, 'done, append result to dst.');
                var rstr = result.map(function(r) { return JSON.stringify(r); } ).join('\n') + '\n';
                fs.appendFile(state.dst, rstr, function(err) {
                  if (err) {
                    log.error('failed write result', file, 'will addto retry list.');
                    state.retry.push(file);
                    next();
                  } else {
                    log.info('write result done', file);
                    next();
                  }
                });
              });
            });
          });
          async.parallelLimit(jobs, conf['parallelLimit'] || 10, function(err, results) {
            if (err) {
              return next(err);
            }
            next();
          });
        },
        // if there is retry items, write them out
        function(next) {
          if (state.retry.length > 0) {
            log.info('output retry.sh.');
            var s = util.format('echo { "name": "%s", "filelist": %s } | ./bin/yelp extract', conf['name'], JSON.stringify(state.retry));
            fs.writeFile('retry.sh', s, { mode: 493 }, next);
          } else {
            log.info('no retry items, skip output retry.sh.');
          }
        }
      ]});
};
