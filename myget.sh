#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ret=
COUNTER=0
while [  $COUNTER -lt 3 ]; do
  $DIR/myget/phantomjs/bin/phantomjs --load-images=false $DIR/myget/myget.js $@
  ret=$?
  if [ $ret == 1 ]; then
    let COUNTER=COUNTER+1
  else
    break
  fi
done
exit $ret # out of retry
