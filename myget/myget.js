/*
   use webproxy to get some web page
   return:
     0 -- ok
     1 -- timeout
     2 -- banned
 */

var system = require('system');
var page = new WebPage(),
    url = system.args[1],
    dst = system.args[2];

console.error = function () {
  system.stderr.write(Array.prototype.join.call(arguments, ' ') + '\n');
};

function waitFor(testFx, onReady, timeOutMillis) {
  var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
  start = new Date().getTime(),
  condition = false,
  interval = setInterval(function() {
    if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
      // If not time-out yet and condition not yet fulfilled
      condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
    } else {
      if(!condition) {
        // If condition still not fulfilled (timeout but condition is 'false')
        console.error("'waitFor()' timeout");
        phantom.exit(1);
      } else {
        // Condition fulfilled (timeout and/or condition is 'true')
        console.error("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
        typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
        clearInterval(interval); //< Stop this interval
      }
    }
  }, 250); //< repeat check every 250ms
}

var pageRender = (function() {
  var count = 0;
  return function(page) {
    page.render('' + count + '.png');
    count += 1;
  }
})();

/**
 * From PhantomJS documentation:
 * This callback is invoked when there is a JavaScript console. The callback may accept up to three arguments:
 * the string for the message, the line number, and the source identifier.
 */
page.onConsoleMessage = function (msg, line, source) {
  console.error('console> ' + msg);
};

/**
 * From PhantomJS documentation:
 * This callback is invoked when there is a JavaScript alert. The only argument passed to the callback is the string for the message.
 */
page.onAlert = function (msg) {
  console.error('alert!!> ' + msg);
};

page.onError = function(msg, trace) {
  console.error('error!!> ' + msg);
};

page.onUrlChanged = function(targetUrl) {
  console.error('New URL: ' + targetUrl);
};

page.onLoadFinished = function(status) {
  console.error('Status: ' + status, page.frameUrl);
  if ((/page\.php/gi).test(page.frameUrl)) {
    //pageRender(page);
    if (page.content.indexOf('Sorry, you\'re not allowed to access this page.') >= 0) {
      // yes, we hit the wall
      phantom.exit(2);
    }
    console.log(page.content);
    phantom.exit(0);
  }
};

//page.onResourceRequested = function(requestData, request) {
//  console.error('will request:', requestData['url']);
//};

// Callback is executed each time a page is loaded...
page.open(url, function (status) {
  // State is initially empty. State is persisted between page loads and can be used for identifying which page we're on.
  waitFor(function() {
    return page.evaluate(function() {
      return $('form').is(":visible");
    });
  }, function() {
    initialize(url);
  });
});

setTimeout(function() {
  // we will exit after fixed time (5min), this is the timeout
  phantom.exit(1);
}, 5 * 60 * 1000);

// Step 1

function initialize(url) {
  var r = page.evaluate(function(url, dst) {
    // available engines
    var initConf = {
      'http://www.proxylistpro.com': function(dst) {
        //console.error('now action is:', $('#web-form form').attr('action'));
        $('#web-form form input#address').val(dst);
        $('form').submit();
        //console.error('Searching...');
      },
      'http://www.proxfree.com': function(dst) {
        //console.error('now form is:', $('form'), $('form input[name="get"]'));
        $('form input[name="get"]').val(dst);
        var allOpts = $('form select#pfserverDropdown option');
        var selected = Math.floor((Math.random() * allOpts.length) + 1);
        var actionValue = $(allOpts[selected]).attr('value');
        $('form').attr('action', actionValue).submit();
      }
    };

    if (url in initConf) {
      return initConf[url](dst);
    } else {
      console.error(url, 'is not configured. Exit!');
      return 'error'
    }
  }, url, dst);
  if (r === 'error') {
    phantom.exit(2);
  }
  //pageRender(page);
}

